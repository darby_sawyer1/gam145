// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Activity_oneGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ACTIVITY_ONE_API AActivity_oneGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
